﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VideoPlain.aspx.cs" Inherits="WelyAspnetWebForm.VideoPlain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript">  var appInsights=window.appInsights||function(config){    function i(config){t[config]=function(){var i=arguments;t.queue.push(function(){t[config].apply(t,i)})}}var t={config:config},u=document,e=window,o="script",s="AuthenticatedUserContext",h="start",c="stop",l="Track",a=l+"Event",v=l+"Page",y=u.createElement(o),r,f;y.src=config.url||"https://az416426.vo.msecnd.net/scripts/a/ai.0.js";u.getElementsByTagName(o)[0].parentNode.appendChild(y);try{t.cookie=u.cookie}catch(p){}for(t.queue=[],t.version="1.0",r=["Event","Exception","Metric","PageView","Trace","Dependency"];r.length;)i("track"+r.pop());return i("set"+s),i("clear"+s),i(h+a),i(c+a),i(h+v),i(c+v),i("flush"),config.disableExceptionTracking||(r="onerror",i("_"+r),f=e[r],e[r]=function(config,i,u,e,o){var s=f&&f(config,i,u,e,o);return s!==!0&&t["_"+r](config,i,u,e,o),s}),t    }({        instrumentationKey:"67763ba3-e805-4fed-bb14-b04898b62844"    });           window.appInsights=appInsights;    appInsights.trackPageView();</script>
    <title></title>
    <link href="//amp.azure.net/libs/amp/latest/skins/amp-default/azuremediaplayer.min.css" rel="stylesheet" />
    <script src= "//amp.azure.net/libs/amp/latest/azuremediaplayer.min.js"></script>
    <script src="amp-appInsights.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <video id="azuremediaplayer" class="azuremediaplayer amp-default-skin amp-big-play-centered"> </video>
    <script>
      var myOptions = {
        "nativeControlsForTouch": false,
        autoplay: true,
        controls: true,
        width: "640",
        height: "400",
        poster: "",
        plugins: {
            appInsights: {
                //add additonal plugin options here
                'debug': true
            }
        }
      };
      var myPlayer = amp("azuremediaplayer", myOptions);
      myPlayer.src([
        {src: "http://amssamples.streaming.mediaservices.windows.net/91492735-c523-432b-ba01-faba6c2206a2/AzureMediaServicesPromo.ism/manifest", type: "application/vnd.ms-sstr+xml"}, 
      ]);
    </script>
    </form>
</body>
</html>
