﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace WelyAspnetWebForm
{
    public partial class HybridConnectionDemo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            List<string> customerList = new List<string>();
            using (SqlConnection con = new SqlConnection(txtConnectionString.Text))
            {
                con.Open();
                string cmdText = "SELECT Top 10 Name from Customer";
                using (SqlCommand cmd = new SqlCommand(cmdText, con))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        customerList.Add(reader["Name"].ToString());
                    }
                    GridView1.DataSource = customerList;
                    GridView1.DataBind();
                }
            }
        }
    }
}